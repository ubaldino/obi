#!/bin/bash

TITLE="Slideshow tester"

SOURCE=.
BUILD=$SOURCE/build
SOURCESLIDES=$SOURCE/slideshows

mv "$BUILD" "$BUILD.backup" 2>/dev/null
trap "[ -e "$BUILD.backup" ] && rm -rf "$BUILD" ; mv "$BUILD.backup" "$BUILD"" 0 1 2 15
make build_ubuntu | tee | zenity --progress --pulsate --title="$TITLE" --text="testing.\n<i>(make build_ubuntu)</i>" --auto-close
./Slideshow.py --path="$BUILD/ubuntu" --controls
