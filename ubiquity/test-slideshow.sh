#!/bin/bash

TITLE="Abel devel"

SOURCE=.
BUILD=$SOURCE/build

mv "$BUILD" "$BUILD.backup" 2>/dev/null
trap "[ -e "$BUILD.backup" ] && rm -rf "$BUILD" ; mv "$BUILD.backup" "$BUILD"" 0 1 2 15
make build_fosOBI | tee | zenity --progress --pulsate --title="$TITLE" --text="testing.\n<i>(make build_ubuntu)</i>" --auto-close
./Slideshow.py --path="$BUILD/fosOBI" --controls
